# Installation #
* Activate the Bundle
in app\AppKernel.php

```
#!php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            ....
            new Webbamboo\NewsletterBundle\NewsletterBundle(),
        );
        ......
    }
}

```
* Add the routing
in app\config\routing.yml

```
#!yml

newsletter:
    resource: "@NewsletterBundle/Resources/config/routing.yml"
    prefix:   /
```
* Update Doctrine schema

```
#!bash

php app/console doctrine:schema:update --force
```

# Configuration #
* in Webbamboo\NewsletterBundle\Resources\config\parameters.yml

```
#!yml

parameters:
    newsletter_from_email: default@email.com
    
```
Change the from email to the one you need

*  Edit the email templates in Webbamboo\NewsletterBundle\Resources\views\
# Usage #
Add a newsletter subscribe form to the views
Example:
```
#!html

<div class="input-group">
    <input type="text" class="form-control" placeholder="Enter your email here" id="newsletterEmail">
    <span class="input-group-btn">
        <button class="btn btn-info btn-newsletter" type="button">
            <b>Okay</b>
        </button>
        <button class="btn btn-warning" style="display:none" id="loadingButton"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></button>
    </span>
</div><!-- /input-group -->
    
```
And some JS for the ajax
```
#!js
$(function() {
        var token = "{{ default_csrf_token() }}";
        $(".btn-newsletter").click(function(){
            $(".btn-newsletter").hide();
            $("#loadingButton").show();
            $.post( "{{ path('newsletter_subscribe') }}", {
                token: token,
                email: $("#newsletterEmail").val()
            })
            .done(function( data ) {
                $(".btn-newsletter").show();
                $("#loadingButton").hide();
                $("#newsletterEmail").val("");
                if(typeof data.error !== 'undefined')
                {
                    swal({
                        title: "Error!",
                        text: data.error,
                        type: "error",
                        confirmButtonText: "Ok"
                    });
                }
                if(typeof data.success !== 'undefined')
                {
                    swal({
                        title: "Thanks for subscribing",
                        text: data.success,
                        type: "success",
                        confirmButtonText: "Ok"
                    });
                }
            });
        });
        {% if app.request.query.get("newsletterSubscribe") %}
            swal({
                title: "Subscription Confirmed",
                text: "Thank you for subscribing! \r\nWe'll let you know as soon as we're online.",
                type: "success",
                confirmButtonText: "Ok"
            });
        {% endif %}
        {% if app.request.query.get("newsletterUnsubscribe") %}
            swal({
                title: "Unsubscribe Successful",
                text: "We are sorry to see you go!",
                type: "success",
                confirmButtonText: "Ok"
            });
        {% endif %}
    });

```