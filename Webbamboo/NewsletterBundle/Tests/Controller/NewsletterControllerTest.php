<?php

namespace Webbamboo\NewsletterBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NewsletterControllerTest extends WebTestCase
{
    public function testSubscribe()
    {
        $client = static::createClient();

        $client->enableProfiler();

        $crawler = $client->request('POST', '/subscribe', array(
            'token' => $client->getContainer()->get('webbamboo.twig.csrf_twig_extension')->getCsrfToken(),
            'email' => 'pavel.petrov.wb@gmail.com'
        ));

        $mailCollector = $client->getProfile()->getCollector('swiftmailer');

        // Check that an email was sent
        $this->assertEquals(1, $mailCollector->getMessageCount());

        $collectedMessages = $mailCollector->getMessages();
        $message = $collectedMessages[0];

        // Asserting email data
        $this->assertInstanceOf('Swift_Message', $message);
        $this->assertEquals('Confirm your e-mail address', $message->getSubject());
        $this->assertEquals($client->getContainer()->getParameter('newsletter_from_email'), key($message->getFrom()));
        $this->assertEquals('pavel.petrov.wb@gmail.com', key($message->getTo()));

        $response = $client->getResponse();
        $this->assertSame(200, $client->getResponse()->getStatusCode()); // Test if response is OK
        $this->assertSame('application/json', $response->headers->get('Content-Type')); // Test if Content-Type is valid application/json
        $this->assertEquals('{"success":"We need to confirm your email address.\r\n\r\nTo complete the subscription process, please click the link in the email we just sent you."}', $response->getContent()); // Test the JSON
        $this->assertNotEmpty($client->getResponse()->getContent()); // Test that response is not empty
    }

    public function testVerify()
    {
        $client = static::createClient();


        $client->enableProfiler();


        $crawler = $client->request('GET', '/verify/uoyvt6mQEopTM8rVbzZ5QT1H0e9jLi8fGHKaO70twpmiyOdJod');

        $mailCollector = $client->getProfile()->getCollector('swiftmailer');

        // Check that an email was sent
        $this->assertEquals(1, $mailCollector->getMessageCount());

        $collectedMessages = $mailCollector->getMessages();
        $message = $collectedMessages[0];

        // Asserting email data
        $this->assertInstanceOf('Swift_Message', $message);
        $this->assertEquals('Newsletter Subscribe!', $message->getSubject());
        $this->assertEquals($client->getContainer()->getParameter('newsletter_from_email'), key($message->getFrom()));
        $this->assertEquals($client->getContainer()->getParameter('newsletter_from_email'), key($message->getTo()));

        $this->assertSame(301, $client->getResponse()->getStatusCode()); // Test if response is OK
    }

    public function testUnsubscribe()
    {
        $client = static::createClient();


        $client->enableProfiler();


        $crawler = $client->request('GET', '/unsubscribe/uoyvt6mQEopTM8rVbzZ5QT1H0e9jLi8fGHKaO70twpmiyOdJod');

        $mailCollector = $client->getProfile()->getCollector('swiftmailer');

        // Check that an email was sent
        $this->assertEquals(1, $mailCollector->getMessageCount());

        $collectedMessages = $mailCollector->getMessages();
        $message = $collectedMessages[0];

        // Asserting email data
        $this->assertInstanceOf('Swift_Message', $message);
        $this->assertEquals('Newsletter Unsubscribe!', $message->getSubject());
        $this->assertEquals($client->getContainer()->getParameter('newsletter_from_email'), key($message->getFrom()));
        $this->assertEquals($client->getContainer()->getParameter('newsletter_from_email'), key($message->getTo()));

        $this->assertSame(301, $client->getResponse()->getStatusCode()); // Test if response is OK
    }
}
