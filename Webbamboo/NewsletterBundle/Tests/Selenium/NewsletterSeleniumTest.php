<?php
namespace Webbamboo\NewsletterBundle\Tests\Selenium;
class NewsletterSeleniumTest extends AbstractSeleniumTest
{
    public function testSubscribe()
    {
        $data = array(
            'email'  => 'pavel.petrov.wb@gmail.com'
        );
        $url = $this->router->generate('index');
        $this->open($url);
        $this->type('newsletterEmail',  $data['email' ]);
        $this->click("//button[@type='button']");
        $this->waitForPageToLoad(30000);
        $this->assertTrue($this->isTextPresent('Thanks for subscribing'));
    }
}
