<?php
namespace Webbamboo\NewsletterBundle\Tests\Selenium;
use
    Symfony\Bundle\FrameworkBundle\Console\Application,
    Symfony\Component\Console\Input\ArrayInput
    ;
abstract class AbstractSeleniumTest extends \PHPUnit_Extensions_SeleniumTestCase
{
    protected $kernel;
    protected $router;
    //protected $em;
    protected function setUp()
    {
        parent::setUp();
        require_once "/ssd/apache2/instaserp/app/AppKernel.php";
        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();
        $this->router = $this->kernel->getContainer()->get('router');
        $this->router->getContext()->setHost('instaserp.loc');//Local dev domain
        $this->router->getContext()->setBaseUrl($_SERVER['SCRIPT_NAME']);
        $this->setHost('localhost'); // Set the hostname for the connection to the Selenium server.
        $this->setPort(4444); // set port # for connection to selenium server
        $this->setBrowser('*firefox');
        $this->setBrowserUrl('http://instaserp.loc/');
    }
}
