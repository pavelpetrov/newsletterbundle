<?php
// src/Webbamboo/AndroidrecoveryBundle/EventListener/DateUpdater.php
namespace Webbamboo\NewsletterBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Webbamboo\NewsletterBundle\Entity\Newsletter;

class NewsletterSecret
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();
        if ($entity instanceof Newsletter) {
            $entity->setCreated((new \DateTime('now')));
            $entity->setSecret($this->getSecret($entityManager));
            $entity->setVerified(false);
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();
        if ($entity instanceof Newsletter) {
            if(!$entity->getCreated())
            {
                $entity->setCreated((new \DateTime('now')));
            }
            $entity->setUpdated((new \DateTime('now')));
        }
    }

    private function getSecret($em)
    {
        $unique = false;
        while(!$unique)
        {
            $secret = $this->generateSecret();
            if(!$em->getRepository('NewsletterBundle:Newsletter')->findOneBySecret($secret))
            {
                $unique = true;
            }
        }
        return $secret;
    }

    private function generateSecret($length=50)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
