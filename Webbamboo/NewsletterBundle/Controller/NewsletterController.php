<?php

namespace Webbamboo\NewsletterBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\HttpFoundation\JsonResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Doctrine\Common\Collections\Criteria;

use Webbamboo\NewsletterBundle\Entity\Newsletter;

/**
 * Newsletter controller.
 *
 * @Route()
 */
class NewsletterController extends Controller {
    /**
     * Subscribe
     *
     * @Route("/subscribe", name="newsletter_subscribe")
     * @Method("POST")
     */
    public function subscribeAction(Request $request) {
        $token = $request->request->get('token');
        $email = $request->request->get('email');
        //Verify CSRF
        if(!$this->isCsrfTokenValid('default', $request->get('token')))
        {
            return new JsonResponse(array('error' => 'There was an error! Please refresh the page and try again.'));
        }
        //Validate the email
        $emailConstraint = new EmailConstraint();
        $emailConstraint->message = 'The email you submitted seems to be invalid :(';

        $errors = $this->get('validator')->validate(
            $email,
            $emailConstraint
        );
        if($errors->count() !== 0 || empty($email))
        {
            return new JsonResponse(array('error' => 'The email you submitted seems to be invalid :('));
        }
        else
        {
            $newsletter = new Newsletter();
            $newsletter->setEmail($email);
            $em = $this->getDoctrine()->getManager();
            $em->persist($newsletter);
            $em->flush();
            $this->sendVerificationMail($email, $newsletter->getSecret());
            //var_dump($errors);
            return new JsonResponse(array('success' => "We need to confirm your email address.\r\n\r\nTo complete the subscription process, please click the link in the email we just sent you."));
        }
    }

    private function sendVerificationMail($email, $secret)
    {

        $subject = "Confirm your e-mail address";
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->container->getParameter('newsletter_from_email'))
            ->setTo(array($email))
            ->setReplyTo($this->container->getParameter('newsletter_from_email'))
            ->setBody(
                $this->renderView(
                    'NewsletterBundle:Newsletter:subscribe.email.html.twig',
                    array('secret' => $secret)
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);
    }

    /**
     * Verify an email
     *
     * @Route("/verify/{secret}", name="newsletter_verify")
     * @Method("GET")
     */
    public function verifyAction($secret) {
        $em = $this->getDoctrine()->getManager();
        $newsletter = $em->getRepository('NewsletterBundle:Newsletter')->findOneBySecret($secret);
        if(!$newsletter)
        {
            return $this->redirectToRoute('index', array(), 301);
        }
        else
        {
            $newsletter->setVerified(true);
            $em->flush();
            $subject = "Newsletter Subscribe!";
            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->container->getParameter('newsletter_from_email'))
                ->setTo(array($this->container->getParameter('newsletter_from_email')))
                ->setReplyTo($this->container->getParameter('newsletter_from_email'))
                ->setBody(
                    $this->renderView(
                        'NewsletterBundle:Newsletter:success.email.html.twig',
                        array()
                    ),
                    'text/html'
                )
            ;
            $this->get('mailer')->send($message);
            
            return $this->redirectToRoute('index', array('newsletterSubscribe' => 'true'), 301);

        }


    }
    /**
     * unsubscribe email
     *
     * @Route("/unsubscribe/{secret}", name="newsletter_unsubscribe")
     * @Method("GET")
     * @Template("AndroidrecoveryBundle:Newsletter:unsibscribe.html.twig")
     */
    public function unsubscribeAction($secret) {
        $em = $this->getDoctrine()->getManager();
        $newsletter = $em->getRepository('NewsletterBundle:Newsletter')->findOneBySecret($secret);
        if(!$newsletter)
        {
            return $this->redirectToRoute('index', array(), 301);
        }
        else
        {
            $newsletter->setVerified(false);
            $em->flush();
            $subject = "Newsletter Unsubscribe!";
            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->container->getParameter('newsletter_from_email'))
                ->setTo(array($this->container->getParameter('newsletter_from_email')))
                ->setReplyTo($this->container->getParameter('newsletter_from_email'))
                ->setBody(
                    $this->renderView(
                        'NewsletterBundle:Newsletter:fail.email.html.twig',
                        array()
                    ),
                    'text/html'
                )
            ;
            $this->get('mailer')->send($message);
            
            return $this->redirectToRoute('index', array('newsletterUnsubscribe' => 'true'), 301);
        }
    }
}